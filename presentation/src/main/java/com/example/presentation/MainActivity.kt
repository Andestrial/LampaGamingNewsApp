package com.example.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity


/**
 *  Created by Android Studio on 24.07.2021 15:33
 *  Developer: Artem Prysich
 */

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}