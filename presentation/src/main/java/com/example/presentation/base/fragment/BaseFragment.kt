package com.example.presentation.base.fragment

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding

/**
 *  Created by Android Studio on 24.07.2021 17:26
 *  Developer: Artem Prysich
 */

abstract class BaseFragment<Binding : ViewBinding>(@LayoutRes layoutId: Int) : Fragment(layoutId) {

    protected abstract val binding: Binding

    protected val navController: NavController by lazy { initNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }


    open fun initListeners() {}

    protected open fun initNavController(): NavController = findNavController()
}