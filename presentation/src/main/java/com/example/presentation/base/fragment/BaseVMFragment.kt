package com.example.presentation.base.fragment

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.viewbinding.ViewBinding
import com.example.presentation.base.view_model.BaseViewModel

/**
 *  Created by Android Studio on 24.07.2021 17:24
 *  Developer: Artem Prysich
 */

abstract class BaseVMFragment<Binding : ViewBinding, ViewModel : BaseViewModel>(@LayoutRes layoutId: Int) :
    BaseFragment<Binding>(layoutId) {

    protected abstract val viewModel: ViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
    }




    open fun initObservers() {}
}