package com.example.presentation.base.view_model

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 *  Created by Android Studio on 24.07.2021 17:35
 *  Developer: Artem Prysich
 */

abstract class BaseViewModel : ViewModel() {

    protected val disposables: CompositeDisposable = CompositeDisposable()

    protected infix operator fun CompositeDisposable.plus(d: Disposable) = this.add(d)

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun clearDisposables() {
        disposables.clear()
    }

}