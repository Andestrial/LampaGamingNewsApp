package com.example.presentation.base.recycler_view

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/**
 *  Created by Android Studio on 25.07.2021 15:39
 *  Developer: Artem Prysich
 */

class BaseViewHolder<B : ViewBinding>(val binding: B) : RecyclerView.ViewHolder(binding.root)