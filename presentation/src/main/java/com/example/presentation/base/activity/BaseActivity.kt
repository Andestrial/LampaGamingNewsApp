package com.example.presentation.base.activity

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

/**
 *  Created by Android Studio on 24.07.2021 17:51
 *  Developer: Artem Prysich
 */

abstract class BaseActivity<Binding : ViewBinding>(@LayoutRes layoutId: Int) : AppCompatActivity() {

    protected lateinit var binding: Binding

}