package com.example.presentation.di

import com.example.presentation.screens.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 *  Created by Android Studio on 24.07.2021 18:17
 *  Developer: Artem Prysich
 */

val viewModelsModule = module {

    viewModel { MainViewModel(get()) }
}