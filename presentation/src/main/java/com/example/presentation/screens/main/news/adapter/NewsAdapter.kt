package com.example.presentation.screens.main.news.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.domain.response.NewsResponse
import com.example.presentation.R
import com.example.presentation.base.recycler_view.BaseViewHolder
import com.example.presentation.databinding.ListItemNewsBinding


/**
 *  Created by Android Studio on 25.07.2021 15:33
 *  Developer: Artem Prysich
 */

class NewsAdapter(private val context: Context) :
    RecyclerView.Adapter<BaseViewHolder<ListItemNewsBinding>>() {

    var data: List<NewsResponse> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ListItemNewsBinding> =
        BaseViewHolder(
            ListItemNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: BaseViewHolder<ListItemNewsBinding>, position: Int) {
        with(holder) {
            if (data[position].img?.isNotEmpty() != false) {
                binding.image.visibility = View.VISIBLE
                Glide.with(context)
                    .load(data[holder.adapterPosition].img)
                    .error(R.drawable.ic_baseline_local_fire_department_24)
                    .into(binding.image)
            } else {
                binding.image.visibility = View.GONE
            }


            binding.title.text = data[position].title

            val span = SpannableString(data[position].clickUrl + " - " + data[position].time)
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(data[position].clickUrl))
                    startActivity(context, browserIntent, bundleOf())
                }
            }
            span.setSpan(
                clickableSpan, 0,
                data[position].clickUrl?.length ?: 0,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            span.setSpan(
                ForegroundColorSpan(context.getColor(R.color.accent)),
                0,
                data[position].clickUrl?.length ?: 0,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )

            binding.link.movementMethod = LinkMovementMethod.getInstance();
            binding.link.text = span

        }
    }

    override fun getItemCount(): Int = data.size
}