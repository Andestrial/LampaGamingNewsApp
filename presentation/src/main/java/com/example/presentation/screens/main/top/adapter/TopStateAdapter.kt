package com.example.presentation.screens.main.top.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.domain.response.NewsResponse
import com.example.presentation.screens.main.top.TopFragment

/**
 *  Created by Android Studio on 25.07.2021 21:52
 *  Developer: Artem Prysich
 */
//
class TopStateAdapter(private val fragment: Fragment, private val list: List<NewsResponse>) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = list.size

    override fun createFragment(position: Int): Fragment = TopFragment.create(list[position])

}