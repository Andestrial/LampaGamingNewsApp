package com.example.presentation.screens.main.news

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.domain.response.NewsResponse
import com.example.presentation.R
import com.example.presentation.base.fragment.BaseFragment
import com.example.presentation.databinding.FragmentNewsBinding
import com.example.presentation.screens.main.news.adapter.NewsAdapter
import com.example.presentation.screens.main.top.adapter.TopStateAdapter

/**
 *  Created by Android Studio on 24.07.2021 18:10
 *  Developer: Artem Prysich
 */

class NewsFragment : BaseFragment<FragmentNewsBinding>(R.layout.fragment_news) {

    override val binding: FragmentNewsBinding by viewBinding()

    lateinit var adapter: NewsAdapter

    lateinit var topAdapter: TopStateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = NewsAdapter(requireContext())


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvNews.adapter = adapter
        arguments?.let {
            val list = it.getParcelableArrayList<NewsResponse>("list") ?: emptyList()
            adapter.data = list
            topAdapter = TopStateAdapter(this, list.filter { it.top == 1 })

            binding.topViewPager.adapter = topAdapter
            binding.dotsIndicator.setViewPager2(binding.topViewPager)
        }


    }


    companion object {
        fun create(value: List<NewsResponse>): NewsFragment {
            return NewsFragment().apply {
                arguments = bundleOf(
                    "list" to value
                )
            }
        }
    }
}