package com.example.presentation.screens.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.repository.NewsRepository
import com.example.domain.response.NewsResponse
import com.example.presentation.base.view_model.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 *  Created by Android Studio on 24.07.2021 17:47
 *  Developer: Artem Prysich
 */

class MainViewModel(
    private val newsRepository: NewsRepository
) : BaseViewModel() {

    private val _news = MutableLiveData<List<NewsResponse>>()
    val news: LiveData<List<NewsResponse>> = _news

    private  val allNews : MutableList<NewsResponse> = mutableListOf()

    init {
        getAllNews(0)
        getAllNews(1)
        getAllNews(2)
        getAllNews(3)
    }

    fun getAllNews(page : Int) {
        disposables + newsRepository.getAllNews(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    allNews.addAll(it)
                    _news.postValue(allNews)
                },
                onError = {
                    Log.e("error", it.toString())
                }
            )
    }

    fun search(query : String){
        _news.postValue(allNews.filter { it.title?.contains(query) ?: false })
    }
}