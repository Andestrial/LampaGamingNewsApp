package com.example.presentation.screens.main

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.domain.response.NewsResponse
import com.example.presentation.screens.main.news.NewsFragment

/**
 *  Created by Android Studio on 25.07.2021 19:38
 *  Developer: Artem Prysich
 */

class MainStateAdapter(val fragment: Fragment, private var list: List<NewsResponse>) : FragmentStateAdapter(fragment) {




    override fun getItemCount(): Int = 3


    override fun createFragment(position: Int): Fragment {
        val value = when (position) {
            0 -> list.filter { it.type == "strories" }
            1 -> list.filter { it.type == "video" }
            else -> list.filter { it.type == "favourites" }
        }

        return NewsFragment.create(value)
    }
}