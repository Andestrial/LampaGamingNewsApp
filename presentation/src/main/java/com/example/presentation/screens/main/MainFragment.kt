package com.example.presentation.screens.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.presentation.R
import com.example.presentation.base.fragment.BaseVMFragment
import com.example.presentation.databinding.FragmentMainBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 *  Created by Android Studio on 24.07.2021 17:46
 *  Developer: Artem Prysich
 */

class MainFragment : BaseVMFragment<FragmentMainBinding, MainViewModel>(R.layout.fragment_main) {

    override val viewModel: MainViewModel by viewModel<MainViewModel>()
    override val binding: FragmentMainBinding by viewBinding()

    lateinit var adapter: MainStateAdapter

    private var selectTab: Int? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val searchItem: MenuItem? = binding.toolbar.menu?.findItem(R.id.search)
        val searchView: androidx.appcompat.widget.SearchView? =
            searchItem?.actionView as androidx.appcompat.widget.SearchView

        searchView?.queryHint = "Search..."

        searchView?.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.search(newText ?: "")
                return true
            }
        })
    }

    override fun initListeners() {

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                selectTab = tab?.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
        super.initListeners()
    }


    override fun initObservers() {

        viewModel.news.observe(viewLifecycleOwner) {
            adapter = MainStateAdapter(this, it)
            binding.viewPager.isUserInputEnabled = false
            binding.viewPager.adapter = adapter
            binding.viewPager.setCurrentItem(selectTab ?: 0, false)

            TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                when (position) {
                    0 -> tab.text = "Stories"
                    1 -> tab.text = "Video"
                    2 -> tab.text = "Favorites"
                }
            }.attach()


        }
        super.initObservers()
    }


}