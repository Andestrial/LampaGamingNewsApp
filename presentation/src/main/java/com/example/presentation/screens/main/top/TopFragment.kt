package com.example.presentation.screens.main.top

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.core.os.bundleOf
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.domain.response.NewsResponse
import com.example.presentation.R
import com.example.presentation.base.fragment.BaseFragment
import com.example.presentation.databinding.ListItemSliderBinding


/**
 *  Created by Android Studio on 25.07.2021 21:34
 *  Developer: Artem Prysich
 */

class TopFragment : BaseFragment<ListItemSliderBinding>(R.layout.list_item_slider) {

    override val binding: ListItemSliderBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            val topNews = it.getParcelable<NewsResponse>("item")
            if (topNews?.img?.isNotEmpty() != false) {
                Glide.with(this)
                    .load(topNews?.img)
                    .into(object : CustomTarget<Drawable?>() {
                        override fun onResourceReady(
                            resource: Drawable,
                            transition: Transition<in Drawable?>?
                        ) {
                            binding.root.background = resource
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                        }

                    })
            }


            binding.title.text = topNews?.title

            val span = SpannableString(topNews?.clickUrl + " - " + topNews?.time)
            val clickableSpan: ClickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(topNews?.clickUrl))
                    startActivity(browserIntent)
                }
            }
            span.setSpan(
                clickableSpan, 0,
                topNews?.clickUrl?.length ?: 0,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            span.setSpan(
                ForegroundColorSpan(requireContext().getColor(R.color.accent)),
                0,
                topNews?.clickUrl?.length ?: 0,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )

            binding.link.movementMethod = LinkMovementMethod.getInstance();
            binding.link.text = span
            binding.title.text = topNews?.title

        }
    }

    companion object {
        fun create(value: NewsResponse): TopFragment {
            return TopFragment().apply {
                arguments = bundleOf(
                    "item" to value
                )
            }
        }
    }


}