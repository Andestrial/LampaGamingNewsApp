package com.example.data.source.remote

import com.example.domain.response.NewsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *  Created by Android Studio on 24.07.2021 16:18
 *  Developer: Artem Prysich
 */

interface RemoteDataSource {

    @GET("/")
    fun  getAllNews(
        @Query("page") page : Int
    ) : Single<List<NewsResponse>>
}