package com.example.data.source.remote

import com.example.domain.response.NewsResponse
import io.reactivex.Single
import retrofit2.Retrofit

/**
 *  Created by Android Studio on 24.07.2021 16:21
 *  Developer: Artem Prysich
 */

class RemoteDataSourceImp(retrofit: Retrofit) : RemoteDataSource {

    private val api = retrofit.create(RemoteDataSource::class.java)

    override fun getAllNews(page: Int): Single<List<NewsResponse>> =
        api.getAllNews(page)
}