package com.example.data.repository

import com.example.data.source.remote.RemoteDataSource
import com.example.domain.repository.NewsRepository
import com.example.domain.response.NewsResponse
import io.reactivex.Single

/**
 *  Created by Android Studio on 24.07.2021 18:13
 *  Developer: Artem Prysich
 */

class NewsRepository(
    private val remoteDataSource: RemoteDataSource
) : NewsRepository {

    override fun getAllNews(page: Int): Single<List<NewsResponse>> =
        remoteDataSource.getAllNews(page)


}