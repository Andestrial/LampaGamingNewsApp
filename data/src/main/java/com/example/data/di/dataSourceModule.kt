package com.example.data.di

import com.example.data.source.remote.RemoteDataSource
import com.example.data.source.remote.RemoteDataSourceImp
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *  Created by Android Studio on 24.07.2021 16:01
 *  Developer: Artem Prysich
 */

val dataSourceModule = module {
    fun provideHttpClient(): OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    fun provideGson(): Gson = GsonBuilder()
        .create()

    fun provideConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

    fun provideCallFactory(): CallAdapter.Factory = RxJava2CallAdapterFactory.create()

    fun provideRetrofit(
        httpClient: OkHttpClient,
        callAdapterFactory: CallAdapter.Factory,
        converterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://188.40.167.45:3001")
            .client(httpClient)
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .build()
    }


    // Remote data source
    single { provideHttpClient() }
    single { provideGson() }
    single { provideConverterFactory(get()) }
    single { provideCallFactory() }
    single { provideRetrofit(get(), get(), get()) }
    single<RemoteDataSource> { RemoteDataSourceImp(get()) }

}