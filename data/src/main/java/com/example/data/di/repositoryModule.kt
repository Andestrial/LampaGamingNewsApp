package com.example.data.di

import com.example.data.repository.NewsRepository
import org.koin.dsl.module

/**
 *  Created by Android Studio on 24.07.2021 16:22
 *  Developer: Artem Prysich
 */

val repositoryModule = module {

    single<com.example.domain.repository.NewsRepository> { NewsRepository(get()) }
}