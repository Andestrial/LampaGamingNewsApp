package com.example.lampagamingnewsapp

import android.app.Application
import com.example.data.di.dataSourceModule
import com.example.data.di.repositoryModule
import com.example.presentation.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 *  Created by Android Studio on 24.07.2021 15:28
 *  Developer: Artem Prysich
 */

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@Application)
            modules(
                dataSourceModule,
                repositoryModule,
                viewModelsModule
            )
        }
    }



}