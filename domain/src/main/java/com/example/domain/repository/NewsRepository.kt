package com.example.domain.repository

import com.example.domain.response.NewsResponse
import io.reactivex.Single

/**
 *  Created by Android Studio on 24.07.2021 18:13
 *  Developer: Artem Prysich
 */

interface NewsRepository {

    fun getAllNews(page : Int) : Single<List<NewsResponse>>
}