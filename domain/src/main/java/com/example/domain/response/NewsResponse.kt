package com.example.domain.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsResponse(
    @SerializedName("click_url")
    val clickUrl: String? = "",
    @SerializedName("img")
    val img: String? = "",
    @SerializedName("time")
    val time: String? = "",
    @SerializedName("title")
    val title: String? = "",
    @SerializedName("top")
    val top: Int? = 0,
    @SerializedName("type")
    val type: String? = ""
) : Parcelable